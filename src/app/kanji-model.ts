export class Kanji {
  kanji: string;
  id: number;
  set: string;

  constructor(kanji: string, id: number, set: string) {
    this.kanji = kanji;
    this.id = id;
    this.set = set;
  }
}
